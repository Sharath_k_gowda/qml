import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import com.intimetec.backend 1.0

ApplicationWindow {
    id:root
    width: 840
    height: 480
    visible: true
    title: qsTr("Text-Editor")

    Backend{
        id:backEndId
        onPathChanged: console.log("Path:"+path)
        onDataChanged: console.log("path"+path)
    }

    FileDialog{
        id:openDialogId
        title: "Choose a file"
        selectMultiple: false
        folder: shortcuts.home
        selectExisting: true
        onAccepted: {
            // update the text editor

            backEndId.path=openDialogId.fileUrl;
            editor.text=backEndId.data;

        }

    }

    FileDialog{
        id:saveDialogId
        title: "Choose a file"
        selectMultiple: false
        folder: shortcuts.home
        selectExisting: false
        onAccepted: {
            // update the text backend
            backEndId.path=saveDialogId.fileUrl;
            backEndId.data=editor.text;

        }

    }

    Action{
        id:actionNew
        text: qsTr("New")
        icon.color: "transparent"
        icon.source: "qrc:/flat/oNew.png"
        onTriggered: editor.clear();
    }
    Action{
        id:actionOpen
        text: qsTr("Open")
        icon.color: "transparent"
        icon.source: "qrc:/flat/oOpen.png"
        onTriggered: openDialogId.open()
    }
    Action{
        id:actionSave
        text: qsTr("Save")
        icon.color: "transparent"
        icon.source: "qrc:/flat/oSave.png"
        onTriggered: saveDialogId.open()
    }

    Action{
        id:actionCopy
        text: qsTr("Copy")
        icon.color: "transparent"
        icon.source: "qrc:/flat/oCopy.png"
        onTriggered: editor.copy()
    }

    Action{
        id:actionCut
        text: qsTr("Cut")
        icon.color: "transparent"
        icon.source: "qrc:/flat/oCut.png"
        onTriggered: editor.cut()
    }


    Action{
        id:actionPaste
        text: qsTr("Paste")
        icon.color: "transparent"
        icon.source: "qrc:/flat/oPaste.png"
        onTriggered: editor.paste()
    }
    Action{
        id:actionExit
        text: "Exit"
        icon.color: "transparent"
        icon.source: "qrc:/flat/quit.jpg"
        onTriggered: Qt.quit();
    }

    menuBar: MenuBar{
        Menu{
            id:menuFile
            title: qsTr("File")


            MenuItem{action: actionNew}
            MenuItem{action: actionOpen}
            MenuItem{action: actionSave}
            MenuItem{ action: actionExit}


        }
        Menu{
            id:menuEdit

            title: qsTr("Edit")
            MenuItem{action: actionCopy}
            MenuItem{action: actionCut}
            MenuItem{action: actionPaste}

        }
        Menu{

            title: qsTr("Format")
        }
        Menu{

            title: qsTr("View")
        }
        Menu{

            title: qsTr("Help")
        }
    }

    header: ToolBar{
        Row{
            ToolButton{display: AbstractButton.IconOnly; action: actionNew}
            ToolButton{display: AbstractButton.IconOnly; action: actionOpen}
            ToolButton{display: AbstractButton.IconOnly; action: actionSave}
            ToolButton{display: AbstractButton.IconOnly; action: actionCopy}
            ToolButton{display: AbstractButton.IconOnly; action: actionCut}
            ToolButton{display: AbstractButton.IconOnly; action: actionPaste}
        }
    }


    ScrollView{
        anchors.fill: parent
        TextArea{
            id:editor
            focus: true
            text: "QML is amazing"
            selectByMouse: true
            persistentSelection: true


        }
    }
}
