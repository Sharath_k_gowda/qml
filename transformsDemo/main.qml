import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Transforms")

    Rectangle{
        id:containerId
        anchors.fill: parent
        color: "beige"
        MouseArea{
            anchors.fill: parent
            onClicked: {
                rect1.x=50;
                rect2.rotation=0
                rect3.scale=1
            }


        }
    }

    ClickableRec{
        id:rect1
        width: 100
        height: 100
        transformOrigin: Item.TopLeft
        x:50
        y:100
        color: "yellow"
        onClicked: {
            x=x+20
        }
    }

    ClickableRec{
        id:rect2
        width: 100
        height: 100
        transformOrigin: Item.TopLeft
        x:250
        y:100
        color: "green"
        onClicked: {
            rotation=rotation+15
        }
    }
    ClickableRec{
        id:rect3
        width: 100
        transformOrigin: Item.TopLeft
        height: 100
        x:450
        y:100
        color: "red"
        onClicked: {
            scale=scale+0.05
            rotation=rotation+15
        }
    }

}
