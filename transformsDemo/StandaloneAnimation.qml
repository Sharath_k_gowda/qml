import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Transforms")


    Rectangle{
        id:containerId
        anchors.fill: parent
        color: "beige"

        MouseArea{
            anchors.fill: parent
            onPressed: {
                noAnimationId.start()
                rotationAnimationId.start()

            }
            onReleased: {
                noAnimationId.stop()
                rotationAnimationId.stop()

            }

        }
        Rectangle{
            id:rec
            color: "red"
            width: 100
            height: 100
            x:50
            y:50
            NumberAnimation{
                id:noAnimationId
                target: rec
                property: "x"

                to: 500
                duration: 2000
            }
             RotationAnimation{
                 id:rotationAnimationId
                 target: rec
                 property: "rotation"
                 to: 520
                 duration: 2000

             }


        }
    }


}
