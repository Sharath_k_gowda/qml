import QtQuick 2.15

Rectangle{
    id:root
    signal clicked
    MouseArea{
        anchors.fill: parent
        onClicked: root.clicked()
    }

}
