import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("Animations")
    property bool running: false

    Rectangle{
        id:containerId
        color: "beige"
        anchors.fill: parent
        MouseArea{
            anchors.fill: parent
            onPressed: {

               root.running=true

            }
            onReleased: {
                root.running=false

            }
        }
    }
    Rectangle{
        id:rec
        color: "blue"
        height: 100
        width: 100
        PropertyAnimation on x {
           to: 550
           duration: 2000
           running: root.running


        }
        NumberAnimation on y {
            to: 300
            duration: 2000
            running: root.running
        }
        RotationAnimation on rotation {
            to: 600
            duration: 2000
            running: root.running
        }












    }

}
