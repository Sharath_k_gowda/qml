import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("spin box")
    Column{
        spacing: 20
        width: parent.width
        Label{
            text: "SpinBox Control"
            anchors.horizontalCenter: parent.horizontalCenter
            color: "red"
            font.pointSize: 20
            font.bold: true
        }

        SpinBox
        {  id:spinboxId
            from: 1
            to:100
            value: 20
            stepSize: 5
            anchors.horizontalCenter: parent.horizontalCenter
            editable: true
            onValueChanged: {
                console.log(spinboxId.value)
            }

        }

        Button{
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Capture current value"

            font.pointSize: 10
            font.bold: true
            onClicked: {
                console.log("current value changed to-->"+spinboxId.value)
            }

        }
    }

}
