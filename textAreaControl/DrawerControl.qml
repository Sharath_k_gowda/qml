import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
ApplicationWindow {
    id:rootId
    width: 360
    height: 520
    visible: true
    title: qsTr("Drawer")
    header: ToolBar{
        height: 50
        background: Rectangle{
            color: "cyan"
        }

        RowLayout{
            spacing: 30
            anchors.fill: parent
            ToolButton{
                background: Rectangle{
                    color: "mintcream"
                }
                icon.source: "qrc:/images/menubar.png"

                onClicked: {
                    console.log("tool button clicked")
                    drawerId.open()
                }

            }
            Label{
                id:titleLabelId
                text: "Drawer Demo"
                font.pixelSize: 20
                color: "black"
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true

            }
        }


    }
    Drawer{
        id:drawerId
        width: Math.min(rootId.width,rootId.height)* (2/3);
        height: rootId.height
        interactive: true
        ColumnLayout{
            spacing: 0
            width: parent.width

            Button{
                width: parent.width
                height: 50
                text: "item 1"
                font.pointSize: 20
                background: Rectangle{
                    color: "beige"
                }
                Layout.fillWidth: true
                onClicked: {
                    console.log("Clicked on item 1")
                    drawerId.close();
                }
            }


            Button{
                width: parent.width
                height: 50
                text: "item 2"
                font.pointSize: 20
                background: Rectangle{
                    color: "blue"
                }
                Layout.fillWidth: true
                onClicked: {
                    console.log("Clicked on item 2")
                    drawerId.close();
                }
            }

            Button{
                width: parent.width
                height: 50
                text: "item 3"
                font.pointSize: 20
                background: Rectangle{
                    color: "red"
                }
                Layout.fillWidth: true
                onClicked: {
                    console.log("Clicked on item 3")
                    drawerId.close();
                }
            }



        }

    }
    Rectangle{
        anchors.fill: parent
        color: "lightsteelblue"

    }


}
