import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("text area")

    Column{
        spacing: 40
        width: parent.width
        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            text: "this is TextArea with ScrollView "
            font.pointSize: 20
            font.bold: true
        }

        ScrollView{
            anchors.horizontalCenter: parent.horizontalCenter
            id:viewId
            width: 600
            height: 150
            TextArea{
                id:txtAreaId
                background: Rectangle{
                    color: "white"
                }

                placeholderText: "type something"
                color: "blue"
                wrapMode: TextArea.Wrap
                //text: "Our story began in 2008, when our founders came together around the fact that there was more to life, more to do, and more to give. From the very beginning, our mission has been to create abundance — we just happen to build software.

//Each of the founders has a different story, a different path they took to get here. But at their core, there was alignment. Building In Time Tec wasn’t easy or simple. They had missteps, made sacrifices, and had conflict. Our first office was a dingy basement in Jaipur and it was next to impossible to convince anyone to take a chance on working for us. Most of the founders worked full-time while growing In Time Tec in the evenings."
            }
        }
        Button{
            text: "Submit"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                console.log(txtAreaId.text)
            }
        }
    }
}
