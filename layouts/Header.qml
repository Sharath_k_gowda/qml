import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15

Window {
    width: 600
    height: 600
    visible: true
    title: qsTr("layouts")

    Flow{
        id:flowID
        width: parent.width
        height: parent.height

        Rectangle{
            id:topLeftRecId
            color: "red"
            width: 200
            height: width
            Text {
                anchors.centerIn: parent
                id: textTopLeftId
                text: "Angular"
                font.pointSize: 30
            }


        }
        Rectangle{
            id:topCenterRecId
            color: "purple"
            width: 200
            height: width
            Text {
                anchors.centerIn: parent
                id: texttopCenterRecId
                text: "BCPL"
                font.pointSize: 30
            }

        }
        Rectangle{
            id:topRightRecId
            color: "skyBlue"
            width: 200
            height: width

            Text {
                anchors.centerIn: parent
                id: texttopRightRecId
                text: "C++"
                font.pointSize: 30
            }

        }

        Rectangle{
            id:centerLeftRecId
            color: "cyan"
            width: 200
            height: width
            Text {
                anchors.centerIn: parent
                id: textcenterLeftRecId
                text: "Dart"
                font.pointSize: 30
            }


        }
        Rectangle{
            id:centerCenterRecId
            color: "orange"
            width: 220
            height: width

            Text {
                anchors.centerIn: parent
                id: textcenterCenterRecId
                text: "ECMAScript"
                font.pointSize: 30
            }

        }


        Rectangle{
            id:centerRightRecId
            color: "green"
            width: 200
            height: width
            Text {
                anchors.centerIn: parent
                id: textcenterRightRecId
                text: "Falcon"
                font.pointSize: 30
            }


        }



        Rectangle{
            id:bottomLeftRecId
            color: "pink"
            width: 200
            height: width

            Text {
                anchors.centerIn: parent
                id: textbottomLeftRecId
                text: "Groovy"
                font.pointSize: 30
            }

        }




        Rectangle{
            id:bottomCenterRecId
            color: "blue"
            width: 200
            height: width

            Text {
                anchors.centerIn: parent
                id: textbottomCenterRecId
                text: "HPL"
                font.pointSize: 30
            }

        }

        Rectangle{
            id:bottomRightRecId
            color: "yellow"
            width: 220
            height: width

            Text {
                anchors.centerIn: parent
                id: textbottomRightID
                text: "Informix-4GL"
                font.pointSize: 30
            }

        }


    }



}
