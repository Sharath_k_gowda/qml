import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15

Window {
    width: layoutId.implicitWidth
    height: layoutId.implicitHeight
    visible: true
    title: qsTr("layouts")


    GridLayout{
        anchors.fill: parent
        id:layoutId
        columns: 3
        Rectangle{
            id:topLeftRecId
            color: "red"
            width: 70
            height: width
//            Layout.fillHeight: true
//            Layout.fillWidth: true

        }
        Rectangle{
            id:topCenterRecId
            color: "purple"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }
        Rectangle{
            id:topRightRecId
            color: "skyBlue"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }

        Rectangle{
            id:centerLeftRecId
            color: "cyan"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }
        Rectangle{
            id:centerCenterRecId
            color: "orange"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }


        Rectangle{
            id:centerRightRecId
            color: "green"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }



        Rectangle{
            id:bottomLeftRecId
            color: "pink"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }




        Rectangle{
            id:bottomCenterRecId
            color: "blue"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }

        Rectangle{
            id:bottomRightRecId
            color: "yellow"
            width: 100
            height: width
            Layout.fillHeight: true
            Layout.fillWidth: true


        }

    }
}
