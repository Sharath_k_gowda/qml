#ifndef LOG_H
#define LOG_H
#include<vector>
#include <QObject>
#include <QQuickItem>

class Log :public QObject
{
    Q_OBJECT
public:
    Log();
    static int fileCount;

    enum Status{
        Warn,
        Error,
        Info,
        Debug
    };
    Q_ENUM(Status)
    Q_INVOKABLE void log(Status,const QString);
    static void integrateEnumToQml();

};

#endif // LOG_H
