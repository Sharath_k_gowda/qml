import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import QtWebEngine 1.10
import QtQuick.Layouts 1.15

ApplicationWindow  {
    id:root
    width: 1024
    height: 750
    visible: true
    title: qsTr("QtWebEngine ")

    property url previousURL: ""
    property url nextURL: ""

    header: ToolBar{
        Row{
            ToolButton{
                display: AbstractButton.IconOnly
                action: actionGoBack
                onClicked: {
                    if(webEngineViewId.url!="https://www.google.com"){
                        root.previousURL=webEngineViewId.url;
                        webEngineViewId.url=root.previousURL

                        console.log(root.previousURL)
                    }
                }
            }
            ToolButton{display: AbstractButton.IconOnly; action: actionGoNext}
            ToolButton{
                display: AbstractButton.IconOnly
                action: actionRefresh
                onClicked: {
                    webEngineViewId.url=webEngineViewId.url;
                }
            }
            ToolButton{display: AbstractButton.IconOnly; action: actionAddressBar}
        }


        TextField{
            id: addressBar
            x:160
            width: 1000
            focus: true

            text: webEngineViewId.url
            onAccepted: {
                webEngineViewId.url=addressBar.text
            }

        }

    }
    WebEngineView {
        id:webEngineViewId
        anchors.fill: parent
        url: "https://www.google.com"
    }
    Action{
        id:actionRefresh
        icon.source: "qrc:/icons/view-refresh.png"
        icon.color: "transparent"


    }
    Action{
        id:actionGoBack
        icon.source: "qrc:/icons/go-previous.png"
    }

    Action{
        id:actionGoNext
        icon.source: "qrc:/icons/go-next.png"
    }





}
