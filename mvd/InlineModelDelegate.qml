import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Model view delegate")

    ListView{
        anchors.fill: parent
        id:listViewId
        model: ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        delegate: Rectangle{
            id:rec1
            color: "cyan"
            border.color: "greenyellow"
            width: parent.width
            height:50
            radius: 10
            Text {
                id: textId
                anchors.centerIn: parent
                text: modelData
            }
        }
    }

}
