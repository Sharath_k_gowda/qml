import QtQuick 2.12
Component{
    id:delegateId
    Rectangle{
        color: "cyan"
        border.color: "yellowGreen"
        width: parent.width
        height: 50
        radius: 10
        Text {
            anchors.centerIn: parent
            font.pointSize: 20

            text: name+":"+price
        }
    }

}
