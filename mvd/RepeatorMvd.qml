import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Model view delegate")


    Flickable{
        contentHeight: colId.implicitHeight
        anchors.fill: parent

        Column{
            id:colId

            spacing: 2
            anchors.fill: parent


            Repeater{
                model: ["Sharath","Rahul","Bharath","yash","Jitendra","Rajendra"]
                delegate: Rectangle{
                    id:rec
                    color: "cyan"

                    width: parent.width
                    height: 50
                    Text {
                        id: name
                        anchors.centerIn: parent
                        text: modelData

                    }


                }
            }
        }
    }
}
