import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Model view delegate")

    ListView{
        anchors.fill: parent
        model: listModelId
        delegate: delegateId


    }


    ListModel{
        id:listModelId
        ListElement{
            name:"redmi"
            price:"10000"
        }
        ListElement{
            name:"samsung"
            price:"22000"
        }

        ListElement{
            name:"iphone"
            price:"100000"
        }

        ListElement{
            name:"oneplus"
            price:"50000"
        }

        ListElement{
            name:"vivo"
            price:"34000"
        }
        ListElement{
            name:"oppo"
            price:"54000"
        }
        ListElement{
            name:"realme"
            price:"94000"
        }

    }

    SampleDelegate{
        id:delegateId
    }
}
