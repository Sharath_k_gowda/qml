#ifndef ADDITION_H
#define ADDITION_H

#include <QObject>
#include<QDebug>
#include<QVariant>
class Addition : public QObject
{
    Q_OBJECT
public:
    explicit Addition(QObject *parent = nullptr);

signals:
    void add(QVariant data);

public slots:
    void work();

};

#endif // ADDITION_H
