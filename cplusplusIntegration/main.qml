import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import com.intimetec.addition 1.0
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("C++ with QML")

    Add{
        id:addId
    }

    Rectangle{
        anchors.fill: parent

        color: "#FF851B"
        Text {
            id: textId
            text: ""+addId.work()
        }
        Button{
            id:buttonId
            text: "Click Me"
            anchors.centerIn: parent

            onClicked: {
                addId.work()

            }
        }
    }
}
