import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("anchors part 1")


    Rectangle{
        id:containerRecId
        width: 300
        height: width
        border.color: "black"
        anchors.centerIn: parent
        Rectangle{
            id:topLeftRecId
            color: "magenta"
            width: 100
            height: width

        }

        Rectangle{
            id:topCenterRecId
            color: "yellowgreen"
            width: 100
            height: width
            anchors.left: topLeftRecId.right

        }

        Rectangle{
            id:topRightRecId
            color: "blue"
            width: 100
            height: width
            anchors.left: topCenterRecId.right

        }

        Rectangle{
            id:centerLeftRecId
            color: "red"
            width: 100
            height: width
            anchors.top: topLeftRecId.bottom

        }

        Rectangle{
            id:centerCenterRecId
            color: "green"
            width: 100
            height: width
            anchors.left: centerLeftRecId.right
            anchors.top: topRightRecId.bottom

        }
        Rectangle{
            id:centerRightRecId
            color: "cyan"
            width: 100
            height: width
            anchors.left: centerCenterRecId.right
            anchors.top: topRightRecId.bottom

        }

        Rectangle{
            id:bottomLeftRecId
            color: "skyblue"
            width: 100
            height: width
            anchors.top: centerRightRecId.bottom


        }
        Rectangle{
            id:bottomCenterRecId
            color: "purple"
            width: 100
            height: width
            anchors.left: bottomLeftRecId.right
            anchors.top: centerRightRecId.bottom


        }
        Rectangle{
            id:bottomRightRecId
            color: "grey"
            width: 100
            height: width
            anchors.left: bottomCenterRecId.right
            anchors.top: centerRightRecId.bottom


        }
    }
    Rectangle{
        id:siblingId
        color: "black"
        width: 200
        height: 200
        anchors.right: containerRecId.left
    }

}
