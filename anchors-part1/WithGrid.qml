import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("anchors part 1")


    Rectangle{
        id:containerRecId
        width: 300
        height: width
        border.color: "black"
        anchors.centerIn: parent

        Grid{
            columns: 3
            rowSpacing: 10
            columnSpacing: 10


            Rectangle{
                id:topLeftRecId
                color: "magenta"
                width: 100
                height: width
                Image {
                    id: imgId
                    width: 50
                    height: 50
                    anchors.centerIn: parent
                    source: "qrc:/images/eliphant.jpg"
                }

            }

            Rectangle{
                id:topCenterRecId
                color: "yellowgreen"
                width: 100
                height: width


            }

            Rectangle{
                id:topRightRecId
                color: "blue"
                width: 100
                height: width


            }

            Rectangle{
                id:centerLeftRecId
                color: "red"
                width: 100
                height: width


            }

            Rectangle{
                id:centerCenterRecId
                color: "green"
                width: 100
                height: width


            }
            Rectangle{
                id:centerRightRecId
                color: "cyan"
                width: 100
                height: width


            }

            Rectangle{
                id:bottomLeftRecId
                color: "skyblue"
                width: 100
                height: width



            }
            Rectangle{
                id:bottomCenterRecId
                color: "purple"
                width: 100
                height: width



            }
            Rectangle{
                id:bottomRightRecId
                color: "grey"
                width: 100
                height: width


            }

        }
    }
}
