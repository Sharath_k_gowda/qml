import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
ApplicationWindow {
    id: window
       width: 660
       height: 520
       visible: true

    menuBar: MenuBar {
        // ...
    }

    header: ToolBar {

    }

    footer: TabBar {
        // ...
    }

    StackView {
        anchors.fill: parent
    }
}
