#include "Log.h"
#include<QDebug>
#include<QFile>
#include<QDateTime>
#include<QTextStream>
Log::Log()
{

}
int Log::fileCount=0;


void Log::log(Status logLevel, const QString message){
    QString fileName="logQML";
    int fileSizeLimit=1000;
    int fileLimit=3;
    std::vector<QString> listOflogs;

    listOflogs.push_back("logQML1.txt");//0
    listOflogs.push_back("logQML2.txt");//1
    listOflogs.push_back("logQML3.txt");//2

     qDebug()<<"inside the log fun()";
    QFile file(fileName+".txt");
    if (!file.open(QFile::WriteOnly|QFile::Append)) {
        qDebug()<<"root file is not opened";
        return;
    }
    QDateTime now=QDateTime::currentDateTime();
    const QString time=now.toString("yyyy MM dd HH.mm.ss");

    int fileSize=file.size();
    if (fileSize<=fileSizeLimit) {
        qDebug()<<"checking file size before writing";
        QTextStream out(&file);
        out<<time<<logLevel<<'\t'<<message<<'\n';
        qDebug()<<"file size-->"<<fileSize;
        return;
    }
    if(fileCount>=fileLimit){
        qDebug()<<"checking file count "<<fileCount;
        fileCount--;
        QFile openFile(listOflogs[fileCount]);
        if (openFile.open(QFile::ReadOnly)) {
            openFile.remove();
        }

    }
     qDebug()<<"before forloop";
    for(int i=fileCount;i>0;i--){
        QFile tempFile(listOflogs[i-1]);
        tempFile.open(QFile::ReadOnly);
        tempFile.rename(listOflogs[i]);
    }

    file.rename(listOflogs[0]);
    fileCount++;
    file.close();


}
void Log::integrateEnumToQml(){
    qmlRegisterType<Log> ("Logc",1,2,"Style");
}
