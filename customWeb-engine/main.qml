import QtQuick 2.15
import QtQuick.Window 2.15
import QtWebEngine 1.10
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Web Engine")


    WebEngineView {
        anchors.fill: parent
        url: "qrc:/index.html"


    }
}
