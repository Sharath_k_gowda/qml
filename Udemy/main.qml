import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("example")

    property string showToText: "Click"

    Row{
        id:rowId
        anchors.centerIn: parent
        spacing: 20


        Rectangle{
            id:redRec

            color: "red"
            width: 150
            height: 150
            radius: 20

             MouseArea{
                 anchors.fill: parent

                 onClicked: {
                     showToText="RED"
                     console.log("clicked on Red Rectangle")
                 }
             }
        }

        Rectangle{
            id:greenRec

            color: "green"
            width: 150
            height: 150
            radius: 20
             MouseArea{
                 anchors.fill: parent
                 onClicked: {
                     showToText="GREEN"
                     console.log("clicked on green Rectangle")
                 }
             }
        }


        Rectangle{
            id:blueRec

            color: "blue"
            width: 150
            height: 150
            radius: 20
             MouseArea{
                 anchors.fill: parent

                 onClicked: {
                     showToText="BLUE"
                     console.log("clicked on blue Rectangle")
                 }
             }
        }


        Rectangle{
            id:circle

            color: "darkmagenta"
            width: 150
            height: 150
            radius: 100
            Text {
                id: textID
                anchors.centerIn: parent
                text: showToText
            }
             MouseArea{
                 anchors.fill: parent
                 onClicked: {
                     textID.text=showToText.concat("qml")
                     console.log("clicked on Circle")

                 }
             }
        }




    }
}
