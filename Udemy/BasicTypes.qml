import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("data types")


    Item{
        id:container
        x:50
        y:50
        width: 400
        height: 400
        Rectangle{
            anchors.fill: parent
            color: "cyan"
            border.color: "black"
        }
        Rectangle{
            id:redRec

            color: "red"
            width: 50
            height: 50

        }
        Rectangle{
            id:blueRec
            x:100
            y:40
            color: "blue"
            width: 50
            height: 50

        }


        Rectangle{
            id:greenRec
            x:20
            y:200
            color: "green"
            width: 50
            height: 50

        }
        Text {
            x:100
            y:100
            text: "rahul"
            font.family: "Helvetica"
            font.pointSize: 24
            font.bold: true
            color: "green"

        }
        Text {
            x:200
            y:200
            text: "sharath"
            color: "red"
            font{
                family: "Helvetica"
                pointSize: 24
                bold: true
            }
        }
    }
}
