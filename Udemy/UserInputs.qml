import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("data types")

    Row{
        spacing: 10
        x:10
        y:10

        Rectangle{
            id:firstNameRecId
            width: firstNameLabelId.implicitWidth+20
            height: firstNameLabelId.implicitHeight+20
            color: "beige"
            Text {
                id: firstNameLabelId
                anchors.centerIn: parent
                text: "FirstName:"
            }
        }
        Rectangle{
            id:firstNameTextRecId
            width: firstNameTextId.implicitWidth+20
            height: firstNameTextId.implicitHeight+20
            color: "beige"
            TextInput{
                id:firstNameTextId
                anchors.centerIn: parent
                focus: true
                text: "Enter your first name"
                onEditingFinished: {
                    console.log("the text is-->"+text)
                }
            }
        }

    }

    Row{
        spacing: 10
        x:10
        y:60

        Rectangle{
            id:lastNameRecId
            width: lastNameLabelId.implicitWidth+20
            height: lastNameLabelId.implicitHeight+20
            color: "beige"
            Text {
                id: lastNameLabelId
                anchors.centerIn: parent
                text: "LastName:"
            }
        }
        Rectangle{
            id:lastNameTextRecId
            width: lastNameTextId.implicitWidth+20
            height: lastNameTextId.implicitHeight+20
            color: "beige"
            TextInput{
                id:lastNameTextId
                anchors.centerIn: parent
                focus: true
                text: "Enter your last name"
                onEditingFinished: {
                    console.log("the text is-->"+text)
                }
            }
        }

    }
}
