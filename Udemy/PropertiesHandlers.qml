import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("properties handlers")

    property string firstName: "sharath"
    property int age: 24
    property bool gender: true
    property double s: 5.5
    onSChanged: {

    }



    onGenderChanged: {
        console.log("the value of gender is  "+gender)
    }

    onAgeChanged: {
        console.log("the value of age is  "+age)
    }

    onFirstNameChanged: {
        console.log("the value of firstName is  "+firstName)
    }

    Rectangle{
        id:rec
        color: "greenyellow"
        height: 200
        width: 200
        anchors.centerIn: parent
        MouseArea{
            anchors.fill: parent
            onClicked: {
                firstName="gowda"
                age=26
                gender=false
            }
        }
    }
    Component.onCompleted: {
        console.log("the value of firstName is  "+firstName)
        console.log("the value of age is  "+age)
        console.log("the value of gender is  "+gender)
    }




}
