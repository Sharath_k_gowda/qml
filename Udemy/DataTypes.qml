import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("data types")

    property string name: "sharath"
    property int age: 25
    property bool gender: false
    property double pHeight: 5.8
    property url url1: "https://www.gmail.com"
    property string url2: "https://www.gmail.com"

    property var aNumber: 100
    property var aBool: false
    property var aString: "Hello world!"
    property var anotherString: String("#FF008800")
    property var aColor: Qt.rgba(0.2, 0.3, 0.4, 0.5)
    property var aRect: Qt.rect(10, 10, 10, 10)
    property var aPoint: Qt.point(10, 10)
    property var aSize: Qt.size(10, 10)
    property var aVector3d: Qt.vector3d(100, 100, 100)
    property var anArray: [1, 2, 3, "four", "five", (function() { return "six"; })]
    property var anObject: { "foo": 10, "bar": 20 }
    property var aFunction: (function() { return "one"; })


    property date iDate: "2020-11-17"



    Rectangle{
        id:rec
        color: "red"
        width: 200
        height: 200
        anchors.centerIn: parent

        Text {
            id: textID
            anchors.centerIn: parent
            font.bold: true
            font.pointSize: 20

            text: name
        }

    }
    Component.onCompleted: {


        console.log("the value of name is\t"+name)
        console.log("the value of age is\t"+age)
        console.log("the value of pHeight is \t"+pHeight)
        if(gender){
            console.log("you are male, you have to wear suit!!")
        }
        else{
            console.log("you are female, you have to wear dress!!!")
        }

        if(url1===url2){
            console.log("they are same")
        }else{
            console.log("they are not same")
        }
        if(url1==url2){
            console.log("they are same")
        }else{
            console.log("they are not same")
        }

        console.log("!!!!!!!!!!!!!playing with var data type!!!!!!!!!!!!!!!!1")
        console.log(aNumber)
        console.log(aBool)
        console.log(aColor)
        console.log(aPoint)
        console.log(aSize)
        console.log(aString)

        console.log(aRect)
        console.log(anObject)

        anArray.forEach(function(value,index){
            if(index===5){
                console.log(value());
            }
            else{
                console.log(value)
            }
        })
        // normal for loop
        for(var i=0;i<anArray.length;i++){
            if(i===5){
                console.log(anArray[i]())
            }else{
                console.log(anArray[i])
            }
        }
        console.log("date is\t"+iDate)
        console.log("year is\t"+iDate.getFullYear())
        console.log("time is\t"+iDate.getTime())


        console.log("Function call\t"+aFunction())
    }
}
