import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("property binding")


    Rectangle{
        id:redRec
        color: "red"
        width: 50
        height: width*1.5
        x:100
        y:100


    }
    Rectangle{
        id:blueRec
        color: "blue"
        anchors.bottom: parent.bottom

        width: 100
        height: 100

        MouseArea{
            anchors.fill: parent
            onClicked: {
                redRec.width=redRec.width+10
            }
        }
    }

    Rectangle{
        id:greenRec
        color: "green"
        anchors.bottom: parent.bottom
        anchors.left: blueRec.right

        width: 100
        height: 100

        MouseArea{
            anchors.fill: parent
            onClicked: {
                redRec.height=redRec.height+10
            }
        }
    }







}
