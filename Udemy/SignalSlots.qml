import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("data types")

    Rectangle{
        id:recId
        color: "red"
        width: 100
        height: 100
        anchors.centerIn: parent

        signal greet(string message)
        function myGreet(message){
            console.log("myGreet fun invoked and the avlue is \t"+message)
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                recId.greet("Good morning...!!!")
            }
        }

        Component.onCompleted: {
            recId.greet.connect(recId.myGreet)
        }
    }





}
