import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("TextEdit")
    Flickable{
        width: txt.width
        contentHeight: txt.implicitHeight
        height: parent.height
        anchors.centerIn: parent
        clip: true
        TextEdit {
            id:txt

            width: 240
            text: "<b>Hello</b> <i>Cartos is a cloud-based, fleet design and asset utilization tool for Managed Print Service (MPS) providers. It allows MPS providers to optimize sites, gain more control on print services, and provide more efficient costs, maintenance, and resource utilization.

    Cartos showcases a simple user interface (UI) & workflow for broad user adoption, yet is powerful enough for fleet designers and is designed to coexist with MPS systems & workflow. It allows the visualization and optimization of MPS fleets through every phase of the MPS: pre-sales, fleet design, fleet deployment, service technicians, post-sales, and obsolesce life cycle.!</i>"

            wrapMode: TextEdit.Wrap

            font.family: "Helvetica"
            font.pointSize: 20
            color: "blue"
            focus: true
            textFormat: TextEdit.RichText
        }



    }

}
