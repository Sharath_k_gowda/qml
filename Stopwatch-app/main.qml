import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
// root context
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Stopwatch")

    Connections{
        target: stopwatch
        // this is deprecated
//        onNotice:{
//            labelId.text=data;
//        }
        function onNotice(data){
            labelId.text=data;
        }
    }

    Column{
        id:colId
        width: 228
        height: 159
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 25
        Label{
            id:label1Id
            text: "Stopwatch & Timer"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 22
            font.bold: true
        }

        Label{
            id:labelId
            text: "Status"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 22
            //font.bold: true
        }

        Row{
            id:rowId
            width: 200
            height: 400
            spacing: 50
            Button{
                id:startButtonid
                text: "Start"
                font.bold: true
                palette.button :"cyan"

                onClicked: {
                    stopwatch.start();
                }

            }
            Button{
                id:stopButtonid
                text: "Stop"
                palette.button :"red"
                font.bold: true
                onClicked: {
                    stopwatch.stop()

                }


            }
        }
    }
}
