#include "test.h"

Test::Test(QObject *parent) : QObject(parent)
{
    connect(&m_timer,&QTimer::timeout,this,&Test::timeOut);
    m_timer.setInterval(300);
    m_display="Starting";
    qInfo()<<m_display;
    emit notice(QVariant(m_display));

}

void Test::timeOut()
{
    m_display=QDateTime::currentDateTime().toString();
    qInfo()<<m_display;
    emit notice(QVariant(m_display));

}

void Test::start()
{
    m_display="starting";
    qInfo()<<m_display;
    emit notice(QVariant(m_display));

    m_timer.start();

}

void Test::stop()
{     m_timer.stop();
      m_display="stopped";
        qInfo()<<m_display;
          emit notice(QVariant(m_display));

}
