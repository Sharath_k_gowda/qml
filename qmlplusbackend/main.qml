import QtQuick 2.6
import QtQuick.Window 2.15
import QtQuick.Controls 2.0
import io.qt.examples.backend 1.0

import "demo.js" as Demo


Window {
    id: root
    width: 300
    height: 480
    visible: true


    BackEnd {
        id: backend
    }
    Column{
        anchors.centerIn: parent
        spacing: 10

        TextField {
            text: backend.userName
            placeholderText: qsTr("User name")


            onEditingFinished: backend.userName = text

        }
        Button{
            id:btnId
            palette.button: "blue"
            text: backend.userName
            onClicked: {
                console.log(backend.userName)
                var s= backend.userName;
                Demo.func(s)

            }

        }
        Text {
            id: name
            text: qsTr(backend.userName)
        }
    }
}
