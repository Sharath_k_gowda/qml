import QtQuick 2.0
import QtQuick.Controls 1.0

ApplicationWindow {
    visible: true;
    width: 1000;
    height: 900;

    Rectangle {
        anchors.fill: parent;
        color: '#eeeeee';
    }

    Image {
        anchors.fill: parent
        id: image;
        x: 100;
        y: 100;
        width: 600;
        height: 600;
        source: 'qrc:/images/tiger.jpg';
    }

    SequentialAnimation {
        running: true;
        loops: Animation.Infinite;

        NumberAnimation {
            target: image;
            properties: 'opacity';
            from: 0;
            to: 1;
            duration: 1000;
            easing.type: Easing.OutQuad;
        }

        NumberAnimation {
            target: image;
            properties: 'opacity';
            from: 1;
            to: 0;
            duration: 1000;
            easing.type: Easing.OutQuad;
        }
    }
}
