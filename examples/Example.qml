import QtQuick 2.0
import QtQuick.Window 2.14
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.15
import QtWebEngine 1.10
Window {
    visible: true;
    width: 1400;
    height: 900;
    RowLayout{
        spacing: 20

        WebEngineView{

            implicitHeight: 600
            implicitWidth: 410
            url: "https://www.amazon.in"
            profile: WebEngineProfile{
                httpUserAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1"
            }
        }

        WebEngineView{

            implicitHeight: 600
            implicitWidth: 410
            url: "https://www.udemy.com"
            profile: WebEngineProfile{
                httpUserAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1"
            }
        }
        WebEngineView{

            implicitHeight: 600
            implicitWidth: 410
            url: "https://www.naukri.com/"
            profile: WebEngineProfile{
                httpUserAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1"
            }
        }
    }

}
