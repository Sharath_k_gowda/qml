import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("qml with c++")

    Rectangle{
        id:rec
        color: "beige"
        anchors.fill: parent
         TextField{
             anchors.centerIn: parent
             text: "Num1"
         }

        Button{
            anchors.centerIn: parent
            text: "connect to c++"
            onClicked: {
              console.log("invoked onClicked()");
               testing.fun();
            }

        }
    }

}
