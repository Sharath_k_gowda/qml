import QtQuick 2.0
import QtWebEngine 1.10
import Qt.labs.settings 1.0
import QtQml 2.2
import QtQuick 2.2

import QtQuick.Controls 1.0
import QtQuick.Controls.Private 1.0 as QQCPrivate
import QtQuick.Controls.Styles 1.0
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.1


Window {
    id: window
    property alias currentWebView: webView
    flags: Qt.Dialog | Qt.WindowStaysOnTopHint
    width: 1200
    height: 1000
    visible: true
    //onClosing: destroy()
    property QtObject defaultProfile: WebEngineProfile {
        storageName: "Profile"
        offTheRecord: false
        useForGlobalCertificateVerification: true
    }


    WebEngineView {
        id: webView

        anchors.fill: parent

        url: "http://192.168.1.100:8080/person"
        profile: WebEngineProfile{
            httpUserAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1"
        }

        Component.onCompleted: {
            profile.downloadRequested.connect(function(download){
                download.accept();
            })


        }
    }

    Settings {
        id : appSettings

        property alias devToolsEnabled: devToolsEnabled.checked
        //property alias pdfViewerEnabled: pdfViewerEnabled.checked
    }



    ToolButton {
        id: settingsMenuButton

        Text {
            id: name
            text: qsTr("Settings")

        }
        menu: Menu {

            MenuItem {
                id: devToolsEnabled
                text: "Open DevTools"
                checkable: true
                checked: true


            }



        }

        MenuItem {
            id: loadImages
            text: "Autoload images"
            checkable: true


        }
        MenuItem {
            id: javaScriptEnabled
            text: "JavaScript On"
            checkable: true

        }
        MenuItem {
            id: errorPageEnabled
            text: "ErrorPage On"
            checkable: true

        }
    }


    WebEngineView {
        id: devToolsView
        visible: devToolsEnabled.checked
        height: visible ? 400 : 0
        inspectedView: visible && tabs.currentIndex < tabs.count ? tabs.getTab(tabs.currentIndex).item : null
        anchors.left: parent.left
       anchors.right: parent.right
        anchors.bottom: parent.bottom

//        onNewViewRequested: function(request) {
//            var tab = tabs.createEmptyTab(currentWebView.profile);
//            tabs.currentIndex = tabs.count - 1;
//            request.openIn(tab.item);
//        }

    }
    TabView {
        id: tabs
        function createEmptyTab(profile) {
            var tab = addTab("", tabComponent);
            // We must do this first to make sure that tab.active gets set so that tab.item gets instantiated immediately.
            tab.active = true;
            tab.title = Qt.binding(function() { return tab.item.title ? tab.item.title : 'New Tab' });
            tab.item.profile = profile;
            return tab;
        }

//        function indexOfView(view) {
//            for (let i = 0; i < tabs.count; ++i)
//                if (tabs.getTab(i).item === view)
//                    return i
//            return -1
//        }

//        function removeView(index) {
//            if (tabs.count > 1)
//                tabs.removeTab(index)
//            else
//                browserWindow.close();
//        }

        anchors.top: parent.top
        anchors.bottom: devToolsView.top
        //        anchors.left: parent.left
        anchors.right: parent.right
        Component.onCompleted: createEmptyTab(defaultProfile)



        Component {
            id: tabComponent
            WebEngineView {
                id: webEngineView
                focus: true
                profile: WebEngineProfile{
                    httpUserAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1"
                }
                //                onLinkHovered: function(hoveredUrl) {
                //                    if (hoveredUrl ==="")
                //                        hideStatusText.start();
                //                    else {
                //                        statusText.text = hoveredUrl;
                //                        statusBubble.visible = true;
                //                        hideStatusText.stop();
                //                    }
                //                }

                states: [
                    State {
                        name: "FullScreen"
                        PropertyChanges {
                            target: tabs
                            frameVisible: false
                            tabsVisible: false
                        }
                        PropertyChanges {
                            target: navigationBar
                            visible: false
                        }
                    }
                ]
                settings.autoLoadImages: appSettings.autoLoadImages
                settings.javascriptEnabled: appSettings.javaScriptEnabled
                settings.errorPageEnabled: appSettings.errorPageEnabled
                settings.pluginsEnabled: appSettings.pluginsEnabled
                settings.fullScreenSupportEnabled: appSettings.fullScreenSupportEnabled
                settings.autoLoadIconsForPage: appSettings.autoLoadIconsForPage
                settings.touchIconsEnabled: appSettings.touchIconsEnabled
                settings.webRTCPublicInterfacesOnly: appSettings.webRTCPublicInterfacesOnly
                settings.pdfViewerEnabled: appSettings.pdfViewerEnabled

                onCertificateError: function(error) {
                    error.defer();
                    sslDialog.enqueue(error);
                }

                onNewViewRequested: function(request) {
                    if (!request.userInitiated)
                        print("Warning: Blocked a popup window.");
                    else if (request.destination === WebEngineView.NewViewInTab) {
                        var tab = tabs.createEmptyTab(currentWebView.profile);
                        tabs.currentIndex = tabs.count - 1;
                        request.openIn(tab.item);
                    } else if (request.destination === WebEngineView.NewViewInBackgroundTab) {
                        var backgroundTab = tabs.createEmptyTab(currentWebView.profile);
                        request.openIn(backgroundTab.item);
                    } else if (request.destination === WebEngineView.NewViewInDialog) {
                        var dialog = applicationRoot.createDialog(currentWebView.profile);
                        request.openIn(dialog.currentWebView);
                    } else {
                        var window = applicationRoot.createWindow(currentWebView.profile);
                        request.openIn(window.currentWebView);
                    }
                }

                onFullScreenRequested: function(request) {
                    if (request.toggleOn) {
                        webEngineView.state = "FullScreen";
                        browserWindow.previousVisibility = browserWindow.visibility;
                        browserWindow.showFullScreen();
                        fullScreenNotification.show();
                    } else {
                        webEngineView.state = "";
                        browserWindow.visibility = browserWindow.previousVisibility;
                        fullScreenNotification.hide();
                    }
                    request.accept();
                }

                onQuotaRequested: function(request) {
                    if (request.requestedSize <= 5 * 1024 * 1024)
                        request.accept();
                    else
                        request.reject();
                }

                onRegisterProtocolHandlerRequested: function(request) {
                    console.log("accepting registerProtocolHandler request for "
                                + request.scheme + " from " + request.origin);
                    request.accept();
                }

                onRenderProcessTerminated: function(terminationStatus, exitCode) {
                    var status = "";
                    switch (terminationStatus) {
                    case WebEngineView.NormalTerminationStatus:
                        status = "(normal exit)";
                        break;
                    case WebEngineView.AbnormalTerminationStatus:
                        status = "(abnormal exit)";
                        break;
                    case WebEngineView.CrashedTerminationStatus:
                        status = "(crashed)";
                        break;
                    case WebEngineView.KilledTerminationStatus:
                        status = "(killed)";
                        break;
                    }

                    print("Render process exited with code " + exitCode + " " + status);
                    reloadTimer.running = true;
                }

                onWindowCloseRequested: tabs.removeView(tabs.indexOfView(webEngineView))

                onSelectClientCertificate: function(selection) {
                    selection.certificates[0].select();
                }

                onFindTextFinished: function(result) {
                    if (!findBar.visible)
                        findBar.visible = true;

                    findBar.numberOfMatches = result.numberOfMatches;
                    findBar.activeMatch = result.activeMatch;
                }

                onLoadingChanged: function(loadRequest) {
                    if (loadRequest.status === WebEngineView.LoadStartedStatus)
                        findBar.reset();
                }

                Timer {
                    id: reloadTimer
                    interval: 0
                    running: false
                    repeat: false
                    onTriggered: currentWebView.reload()
                }
            }
        }
    }




}
