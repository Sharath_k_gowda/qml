#include "server.h"
#include <QDataStream>
#include <QTcpSocket>

Server::Server(QObject *parent) : QObject(parent)
{
    connect(&m_server, &QTcpServer::newConnection, this, &Server::handleNewConnection);
}

void Server::run()
{
    if (!m_server.listen(QHostAddress::LocalHost, 5555))
        qWarning() << "Could not start the server -> http/proxy authentication dialog"
                      " will not work. Error:" << m_server.errorString();
}

void Server::handleNewConnection()
{
    QTcpSocket *socket = m_server.nextPendingConnection();
    connect(socket, &QAbstractSocket::disconnected, socket, &QObject::deleteLater);
    connect(socket, &QAbstractSocket::readyRead, this, &Server::handleReadReady);
}

void Server::handleReadReady()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket*>(sender());
    Q_ASSERT(socket);
    QByteArray msg = socket->readAll();
    if (msg.contains(QByteArrayLiteral("OPEN_AUTH")))
        socket->write("HTTP/1.1 401 Unauthorized\nWWW-Authenticate: "
                      "Basic realm=\"Very Restricted Area\"\r\n\r\n");
    if (msg.contains(QByteArrayLiteral("OPEN_PROXY")))
        socket->write("HTTP/1.1 407 Proxy Auth Required\nProxy-Authenticate: "
                      "Basic realm=\"Proxy requires authentication\"\r\n\r\n");
}
