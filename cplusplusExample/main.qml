import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import com.intimetec.machine 1.0

Window {
    id:rootId
    width: 640
    height: 480
    visible: true
    title: qsTr("cplusplus example")
    Component.onCompleted: {
        progressBarId.value=machineId.workload;
        btnStart.enabled=true;
        btnStop.enabled=false;
        btnPause.enabled=false;
        btnResume.enabled=false;
    }

    Machine{
        id:machineId
        onStarted: {
            btnStart.enabled=false;
            btnStop.enabled=true;
            btnPause.enabled=true;
            btnResume.enabled=true;
            labelStatus.text="Started";

        }
        onStopped: {
            btnStart.enabled=true;
            btnStop.enabled=false;
            btnPause.enabled=false;
            btnResume.enabled=fasle;
            labelStatus.text="Stopped"
            progressBarId.value=0.0;

        }
        onResumed: {
            btnStart.enabled=true;
            btnStop.enabled=false;
            btnPause.enabled=false;
            btnResume.enabled=fasle;
            labelStatus.text="Resumed"


        }
        onPaused: {
            btnStart.enabled=false;
            btnStop.enabled=true;
            btnPause.enabled=true;
            btnResume.enabled=false;
            labelStatus.text="Paused"

        }
        onProgress: {
            labelStatus.text="Progress: "+machineId.workload+"%"
            progressBarId.value=(machineId.workload*0.02)

        }
    }

    Column{
        id:columnId
        width: 434
        height: 62
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 10
        Label{
            id:labelStatus
            text: "Status"

        }
        ProgressBar{
            id:progressBarId
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter
            value: 0.5


        }
        Row{
            id:rowId
            width: 200
            height: 400
            spacing: 10

            Button{
                id:btnStart
                text: qsTr("Start")
                onClicked: machineId.start();
            }
            Button{
                id:btnPause
                text: qsTr("Pause")
                onClicked: machineId.pause();
            }
            Button{
                id:btnResume
                text: qsTr("Resume")
                onClicked: machineId.resume();
            }
            Button{
                id:btnStop
                text: qsTr("Stop")
                onClicked: machineId.stop();
            }


        }

    }
}
