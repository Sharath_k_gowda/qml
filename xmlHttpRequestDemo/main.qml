import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("downloading Http data")
    property var ourData;
    function ajaxData(){
        var ourRequest=new XMLHttpRequest();
        ourRequest.open('GET','https://learnwebcode.github.io/json-example/animals-1.json');
        ourRequest.onload=function(){
            ourData=ourRequest.responseText;
            console.log(ourData)

        }
        ourRequest.send();
    }

    Button{
        anchors.centerIn: parent
       text: "Get JSON data from web server"

       onClicked: {
           ajaxData()
       }
    }


    Text{
        text:" "+ourData
    }



}
