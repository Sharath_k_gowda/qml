import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("Mouse Area")

    Rectangle{
        id:rec

        width:root.width
        height: 200
        color: "beige"
        Rectangle{
            id:rec1
            color: "red"
            width: 50
            height: 50
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log(mouse.x)
                rec1.x=mouse.x
            }
            hoverEnabled: true
            onHoveredChanged: {
                if(containsMouse==true){
                    rec1.color="green"

                }else{
                    rec1.color="yellow"

                }
            }

        }
    }

    Rectangle{
        y:300
        id:rec2
        color: "blue"
        width: 100
        height: 100
        MouseArea{
            anchors.fill: rec2
            drag.target: rec2
            drag.axis: Drag.XAndYAxis
            drag.minimumX: 0
            drag.maximumX: 800
            drag.minimumY: 0
            drag.maximumY: 800

        }
        Text {
            id: txt
            text: qsTr("Drag me")
            anchors.centerIn: rec2
        }

    }
}
