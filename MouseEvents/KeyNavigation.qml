import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("keys ")

    Row{
        spacing: 1
        id:row
        anchors.centerIn: parent
        Rectangle{
            id:greenRecId
            color: "green"
            width: 200
            height: 200
            border.color: "black"
            border.width: 3
            focus: true
            Keys.onDigit5Pressed: {
                console.log("this is first Rectangle")
            }
            KeyNavigation.right: redRecId
            onFocusChanged: {
                if(focus==true){
                    color="blue"

                }else{
                     color="black"
                }
            }
        }


        Rectangle{
            id:redRecId
            color: "red"
            width: 200
            height: 200
            border.color: "black"
            border.width: 3

            Keys.onDigit5Pressed: {
                console.log("this is second Rectangle")
            }
            KeyNavigation.left: greenRecId
            onFocusChanged: {
                if(focus==true){
                    color="cyan"

                }else{
                     color="yellow"
                }
            }
        }















    }






}
