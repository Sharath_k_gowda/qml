import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("keys ")

    Rectangle{
        id:rec
        color: "red"
        anchors.centerIn: parent
        width: 200
        height: 100
        focus: true

        Keys.onDigit1Pressed: {
            console.log("pressed key one")
        }
        Keys.onTabPressed: {
            console.log("pressed tab key")
        }
    }

}
