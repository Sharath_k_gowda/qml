import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.3
import QtWebEngine 1.10
Window {
    width: 1024
    height: 750
    visible: true
    title: qsTr("Web engine")

    WebEngineView {
        anchors.fill: parent
        url: "qrc:/index.html"

    }



}
