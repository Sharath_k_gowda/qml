import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("states and transitions")


    Rectangle{
        id:containerRecId
        anchors.fill: parent

        Rectangle{
            id:sky
            width: parent.width
            height: 200
            color: "blue"
        }
        Rectangle{
            id:ground
            width: parent.width
            color: "lime"
            anchors.top: sky.bottom
            anchors.bottom: parent.bottom
        }

        Image {
            id: treeSpringId
            source: "qrc:/images/spring.png"
            x: 50
            y: 100
            width: 200
            height: 300

        }


        Image {
            id: treeSummerId
            source: "qrc:/images/summer.jpg"
            x: 50
            y: 100
            width: 200
            height: 300

        }
        Rectangle{
            id:sun
            color: "yellow"
            width: 100
            height: 100
            radius: 60
            x:parent.width-width-100
            y: 50


        }
        state: "spring"
        states: [
            State {
                name: "summer"
                PropertyChanges {
                    target: sky
                    color:"lightblue"

                }
                PropertyChanges {
                    target: treeSummerId
                    opacity:1

                }
                PropertyChanges {
                    target: treeSpringId
                    opacity:0

                }
                PropertyChanges {
                    target: ground
                    color:"darkkhaki"


                }
                PropertyChanges {
                    target: sun
                    color:"yellow"

                }

            },
            State {
                name: "spring"
                PropertyChanges {
                    target: sky
                    color:"deepskyblue"

                }
                PropertyChanges {
                    target: treeSummerId
                    opacity:0

                }
                PropertyChanges {
                    target: treeSpringId
                    opacity:1

                }
                PropertyChanges {
                    target: ground
                    color:"lime"


                }
                PropertyChanges {
                    target: sun
                    color:"lightyellow"

                }


            }

        ]
        MouseArea{
            anchors.fill: parent
            onClicked: {
                containerRecId.state=(containerRecId.state=="spring"?"summer":"spring")
            }
        }

        transitions: [

            Transition {
                from: "summer"
                to: "spring"

                ColorAnimation {

                    duration: 500
                }
                NumberAnimation{
                    duration: 500
                    properties: "opacity"

                }

            },
            Transition {
                from: "spring"
                to: "summer"

                ColorAnimation {

                    duration: 500
                }
                NumberAnimation{
                    duration: 500
                    properties: "opacity"

                }

            }

        ]

    }
}
