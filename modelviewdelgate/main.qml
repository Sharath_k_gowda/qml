import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("MVD")


    ListView {
        anchors.fill: parent
        width: 180; height: 200

        contentWidth: 320
        flickableDirection: Flickable.AutoFlickDirection

        model: ContactModel {}
        delegate: Row {
            Text { text: '<b>Name:</b> ' + name; width: 160 }
            Text { text: '<b>Number:</b> ' + number; width: 160 }
        }
    }




}
