import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("MVD")

    ListModel{
        id:modelId
        ListElement{
            firstName:"sharath"; lastName:"gowda"
        }
        ListElement{
            firstName:"bharath"; lastName:"gowda"
        }
        ListElement{
            firstName:"rahul"; lastName:"Sawan"
        }
        ListElement{
            firstName:"rahul"; lastName:"sinare"
        }
    }

    ColumnLayout{
        anchors.fill: parent
        ListView{
            id:listViewId
            model:modelId
            delegate:delegateId
            Layout.fillHeight:true
            Layout.fillWidth: true
        }

        Button{
            text: "Add item"
            Layout.fillWidth: true
            onClicked: {
                modelId.append({"firstName":"jitendra","lastName":"Sen"})

            }
        }
        Button{
            text: "Clear"
            Layout.fillWidth: true
            onClicked: {
                modelId.clear()
            }
        }
        Button{
            text: "Delete item at index 2"
            Layout.fillWidth: true
            onClicked: {
                if(2<listViewId.model.count){
                    modelId.remove(2,1)
                }
                else{
                    console.log("invalid index")
                }


            }
        }
        Button{
            text: "Set item at index 2"
            Layout.fillWidth: true
            onClicked: {
                modelId.set(2,{"firstName":"Rahul","lastName":"Sawan"})

            }
        }
    }

    Component{
        id:delegateId
        Rectangle{
            width: parent.width
            height: 50
            color: "cyan"
            border.color: "greenyellow"
            radius: 14
            Text {
                id: textId
                text: firstName+"::"+lastName
                anchors.centerIn: parent
                font.pointSize: 20
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    console.log("clicked on\t"+firstName+" "+lastName)
                }
            }
        }

    }


}
