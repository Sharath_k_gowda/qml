import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Flickable and Scroll bar")

    Flickable{
        width: parent.width
        height: parent.height

        contentHeight: colId.implicitHeight
        ScrollBar.vertical: ScrollBar{}

        Column{
            id:colId
            anchors.fill: parent
            Rectangle{

                color: "red"
                width: parent.width
                height: 200
                Text {
                    anchors.centerIn: parent
                    text: "Elemnet 1"
                      font.pointSize: 30
                }


            }
            Rectangle{

                color: "blue"
                width: parent.width
                height: 200
                Text {
                    anchors.centerIn: parent
                    text: "Elemnet 2"
                      font.pointSize: 30
                }


            }

            Rectangle{

                color: "cyan"
                width: parent.width
                height: 200
                Text {
                    anchors.centerIn: parent
                    text: "Elemnet 3"
                      font.pointSize: 30
                }


            }




            Rectangle{

                color: "green"
                width: parent.width
                height: 200
                Text {
                    anchors.centerIn: parent
                    text: "Elemnet 4"
                      font.pointSize: 30
                }


            }



            Rectangle{


                color: "pink"
                width: parent.width
                height: 200
                Text {
                    anchors.centerIn: parent
                    text: "Elemnet 5"
                      font.pointSize: 30
                }


            }
            Rectangle{

               color: "yellow"
               width: parent.width
               height: 200
               Text {
                   anchors.centerIn: parent
                   text: "Elemnet 6"
                     font.pointSize: 30
               }
               Rectangle{

                  color: "purple"
                  width: parent.width
                  height: 200

                  Text {
                      anchors.centerIn: parent
                      text: "Elemnet 7"
                      font.pointSize: 30
                  }


               }


            }


        }


    }
}
