import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: "Page with footer"
    Page{
        id:pageId

        anchors.fill: parent
        header: Rectangle{
            width: parent.width
            height: 70
            color: "red"
            Text {
                id: txtId
                text: qsTr("Header")
                anchors.centerIn: parent
                font.pointSize: 20
                font.bold: true
            }

        }

        SwipeView{
            id:swipeViewId
            anchors.fill: parent
            currentIndex: tabBarId.currentIndex
            Image {
                id: img1

                source: "qrc:/images/tiger.jpg"
            }
            Image {
                id: img2
                source: "qrc:/images/eliphant.jpg"
            }
            Image {
                id: img3
                source: "qrc:/images/ghost.jpg"
            }
        }
        footer: TabBar{
            id:tabBarId
            currentIndex: swipeViewId.currentIndex
            TabButton{
                text: "First"
            }
            TabButton{
                text: "Second"
            }

            TabButton{
                text: "Third"
            }


        }

    }

}
