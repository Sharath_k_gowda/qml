import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Switches")
    Frame{
        anchors.centerIn: parent
        Column{
            width: parent.width
            spacing: 20
            Switch{
                text: "Wi-fi"
                anchors.horizontalCenter: parent.horizontalCenter

            }
            Switch{
                text: "geyser"
                anchors.horizontalCenter: parent.horizontalCenter
                onCheckedChanged: {
                    if(checked===true){
                        console.log("Geyser switch is turned ON")

                    }else{
                        console.log("Geyser switch is turned OFF")

                    }
                }

            }
            Switch{
                text: "mixer"
                anchors.horizontalCenter: parent.horizontalCenter

            }
            Switch{
                text: "Bluetooth"
                anchors.horizontalCenter: parent.horizontalCenter
                enabled: false
            }
        }


    }
}
