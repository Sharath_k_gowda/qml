import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Flickable and Scroll bar")

    Column{
        width: parent.width
        spacing: 20
        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Move the Slider"
            color: "red"
            font.pointSize: 20
        }

        Slider{

            anchors.horizontalCenter: parent.horizontalCenter
            from: 1
            to:100
            value: 20
            onValueChanged: {
                progressId.value=value

            }


        }
        ProgressBar{
            anchors.horizontalCenter: parent.horizontalCenter
            id:progressId
            from: 1
            to:100
            value: 20

        }
    }

}
