import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Window {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("easings")
    property int aniDuration: 500

    Flickable{
        anchors.fill: parent
        contentHeight: colId.width
        ColumnLayout{
            id:colId
            spacing: 2
            width: parent.width
            MovingRectangle{
             backBroundColor: "gray"
             startColor: "beige"
             endColor: "blue"
             animationDuration: aniDuration
             easingText: "linear"
             easingType: Easing.Linear
             containerWidth: root.width
            }
            MovingRectangle{
             backBroundColor: "gray"
             startColor: "green"
             endColor: "blue"
             animationDuration: aniDuration
             easingText: "InQuad"
             easingType: Easing.InQuad
             containerWidth: root.width
            }
            MovingRectangle{
             backBroundColor: "gray"
             startColor: "red"
             endColor: "yellow"
             animationDuration: aniDuration
             easingText: "OutQuad"
             easingType: Easing.OutQuad
             containerWidth: root.width
            }

            MovingRectangle{
             backBroundColor: "gray"
             startColor: "black"
             endColor: "greenyellow"
             animationDuration: aniDuration
             easingText: "InOutQuad"
             easingType: Easing.InOutQuad
             containerWidth: root.width
            }



            MovingRectangle{
             backBroundColor: "gray"
             startColor: "white"
             endColor: "purple"
             animationDuration: aniDuration
             easingText: "OutInQuad"
             easingType: Easing.OutInQuad
             containerWidth: root.width
            }


            MovingRectangle{
             backBroundColor: "gray"
             startColor: "#40ff00"
             endColor: "#00ffff"
             animationDuration: aniDuration
             easingText: "InCubic"
             easingType: Easing.InCubic
             containerWidth: root.width
            }
            MovingRectangle{
             backBroundColor: "gray"
             startColor: "#bf00ff"
             endColor: "#808080"
             animationDuration: aniDuration
             easingText: "OutCubic"
             easingType: Easing.OutCubic
             containerWidth: root.width
            }


            MovingRectangle{
             backBroundColor: "gray"
             startColor: "#ff4000"
             endColor: "#ffbf00"
             animationDuration: aniDuration
             easingText: "InOutCubic"
             easingType: Easing.InOutCubic
             containerWidth: root.width
            }

            MovingRectangle{
             backBroundColor: "gray"
             startColor: "skyblue"
             endColor: "cyan"
             animationDuration: aniDuration
             easingText: "OutInCubic"
             easingType: Easing.OutInCubic
             containerWidth: root.width
            }


        }

    }

}
