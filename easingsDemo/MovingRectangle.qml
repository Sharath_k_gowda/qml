import QtQuick 2.15

Item {
    property var backBroundColor
    property var startColor
    property var endColor
    property string easingText
    property var easingType
    property int animationDuration
    property int  containerWidth
    width: containerId.width
    height: containerId.height
    property int finalX: containerId.width-containedRecId.width

    Rectangle{
        id:containerId
        color: backBroundColor
        width: containerWidth
        height: 50
        Text {
            anchors.centerIn: parent
            text: easingText
        }
        Rectangle{
            id:containedRecId
            color: startColor
            width: 50
            height: 50
            border{color: "black";width: 5}
            radius: 10

            MouseArea{
                anchors.fill: parent
                property bool toRight: false
                onClicked: {
                    if(toRight===false){
                        toRight=true
                        noAnimationId.to=finalX
                        noAnimationId.start()

                        colorAnimationId.from=startColor
                        colorAnimationId.to=endColor
                        colorAnimationId.start()

                    }else{
                        toRight=false


                        noAnimationId.to=0
                        noAnimationId.start()

                        colorAnimationId.from=endColor
                        colorAnimationId.to=startColor
                        colorAnimationId.start()

                    }

                }
            }
            NumberAnimation{
                id:noAnimationId
                target: containedRecId
                property: "x"
                easing.type: easingType
                to:finalX
                duration: animationDuration



            }
            ColorAnimation{
                id:colorAnimationId
                target: containedRecId
                from: startColor
                to: endColor
                property: "color"
                duration: animationDuration
            }
            Component.onCompleted: {
                console.log("the width of contained rec is "+parent.width)
            }


        }


    }

}
