import QtQuick 2.15

Item {
    id:rootId

    property alias buttonText: textId.text
    property alias buttonColor: rec.color
    property alias buttonWidth: rec.width
    property string name: "sharath"

    signal buttonClicked


    Rectangle{
        id:rec
        width: textId.implicitWidth+20
        height: textId.implicitHeight+20
        color: "skyblue"

        border.color: "black"
        border.width: 2
        Text {
            anchors.centerIn: rec
            id: textId
            text:""

        }
        MouseArea{
            anchors.fill: rec
            onClicked: {
                //console.log("clicked on "+textId.text)
                rootId.buttonClicked()
            }
        }

    }

}
