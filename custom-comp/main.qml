import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Custom button")
    Column{
        id:colId
        spacing: 10

        CustomButton{
            x:200
            anchors.centerIn: colId
            buttonText: "Login"
            buttonColor: "skyblue"
            name: "rahul"
            onButtonClicked: {
                console.log("Clicked on "+buttonText)
            }

        }
        CustomButton{
            y:170
            buttonText: "Register"
            buttonColor: "red"
            buttonWidth: 65
            onButtonClicked: {
                console.log("Clicked on "+buttonText)
            }


        }
        CustomButton{
            y:290
            buttonText: "Continue"
            buttonColor: "yellow"
            buttonWidth: 64
            onButtonClicked: {
                console.log("Clicked on "+buttonText)
            }

        }
        CustomButton{
            y:420
            buttonText: "Logout"
            buttonColor: "cyan"
            onButtonClicked: {
                console.log("Clicked on "+buttonText)
            }


        }
    }
}
