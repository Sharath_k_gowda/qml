import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("random dinner app")

    property variant places: ["Koramangala","Indiranagar","HSR Layout","Jayanagar"]

    MouseArea{
        id:mouseAreaId
        anchors.fill: parent

        onClicked: {
            sequentialAnimationId.stop()
            fadeOutId.stop()
            labelId.font.pointSize=45
            labelId.opacity=0
            labelId.rotation=0.0
            fadeOutId.start()
        }

        Label{
            id:labelId
            anchors.centerIn: parent
            font.bold: true
            font.pointSize: 25
            text: "Tap me"


        }
    }

    function pick(){
      var i=Math.floor(Math.random()* places.length)
        labelId.text=places[i];
        parallelAnimationId.start()
    }

    SequentialAnimation{
        id:sequentialAnimationId
        running: true
        loops: Animation.Infinite
        NumberAnimation{
            target: labelId
            property: "font.pointSize"
            to: 45
            duration: 1000
        }
        NumberAnimation{
            target: labelId
            property: "font.pointSize"
            to: 25
            duration: 1000
        }


    }
    OpacityAnimator{
        id:fadeOutId
        running: false
        target: labelId
        from: 1
        to: 0
        duration: 1000

        onFinished: pick()
    }

    ParallelAnimation{
        id:parallelAnimationId
        running: false
        NumberAnimation{
            target: labelId
            from: 0
            to: 45
            duration: 2000
            property: "font.pointSize"
        }
        OpacityAnimator{


            target: labelId
            from: 0
            to: 1
            duration: 2000


        }
        RotationAnimation{
            target: labelId
            from: 0
            to: 1
            duration: 2000
            direction: RotationAnimation.Counterclockwise


        }


    }

}
