import QtQuick 2.15
import QtQuick.Window 2.15
import "Utility.js" as Utility
Window {
    id:rootId
    width: 640
    height: 480
    visible: true
    title: qsTr("js external")

    Rectangle{
        id:containerID
        color: "blue"
        width: 200
        height: 200
        anchors.centerIn: parent
        Text {
            id: txtId
            text: "Click me"
            anchors.centerIn: parent

        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                Utility.message()
                var result=Utility.combineAges(33,17)
                console.log("our ages combined yield\t"+result)
                console.log("invoked subtract() from utility3 js file\t"+Utility.subtract(100,50))

            }

        }
    }
}
