import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id:rootId
    width: 640
    height: 480
    visible: true
    title: qsTr("js")


    Rectangle{
        id:containerId
        color: x>300 ?"red":"green"
        width: getHeight()
        height: 100
        onXChanged: {
            console.log("the current value of x is:"+x)
            console.log(getHeight())

        }
        onYChanged: {
            console.log()
        }
        Component.onCompleted: {
            console.log()
        }

        function getHeight(){
            return height*2;
        }

    }


    MouseArea{
        anchors.fill: containerId
        drag.target: containerId
        drag.axis: Drag.XAxis
        drag.minimumX: 0
        drag.maximumX: 800


    }
}
