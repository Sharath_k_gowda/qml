import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.XmlListModel 2.15
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("XML with mvd")

    XmlListModel{
        id:xmlListModelId
        source: "qrc:///xml/employees.xml"
        query: "/employees/employee"
        XmlRole{name: "name"; query: "name/string()"}
        XmlRole{name: "place"; query: "place/string()"}
        XmlRole{name: "designation"; query: "designation/string()"}
        XmlRole{name: "hot"; query: "@hot/string()"}
        XmlRole{name: "year"; query: "year/number()"}

        onStatusChanged: {
            var pre="XmlListModel.";
            if(status===XmlListModel.Ready){console.log(pre+"\tready count.."+count)};
            if(status===XmlListModel.Loading){console.log(pre+"\tLoading...")};
            if(status===XmlListModel.Error){console.log(pre+"Error "+errorString())};
            if(status===XmlListModel.Null){console.log(pre+"Null")};

        }


    }
    ListView{
        id:listViewId
        anchors.fill: parent
        model: xmlListModelId
        delegate: Rectangle{
            color: "beige"
            width: parent.width
            height: 50
            Row{
                spacing: 20
                Text {

                    id: textId
                    text: name
                    font.pointSize: 20
                    font.bold: hot==="true"?true:false

                }
                Text {
                    id: txtId

                    text: place+"\t"+designation+"\t"+year
                    font.pointSize: 16
                    //font.bold: hot==="true"?true:false



                }
            }

        }

    }

}
