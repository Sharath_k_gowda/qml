import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.XmlListModel 2.15
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("XML with mvd")

    XmlListModel {
        id: model
        source: "http://mysite.com/feed.xml"
        query: "/feed/entry"
        XmlRole { name: "title"; query: "title/string()" }
    }

    ListView{
         width: 180; height: 300
        model: model
        delegate: Component{
            Text {

                text: title;
            }
        }

        }
    }
