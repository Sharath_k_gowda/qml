import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true

    title: qsTr("Focus scope")

    Column{


        MyButton{
            focus: true

        }
        MyButton{

            buttonColor: "green"
            focus: true

        }
        MyButton{
            focus: true
        }
    }
}
