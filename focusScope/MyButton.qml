import QtQuick 2.15

FocusScope {
    width: recId.width
    height: recId.height
    property alias buttonColor: recId.color

    Rectangle{
        id:recId
        color: "yellow"
        width: 300
        height: 50
        focus: true
        Text {

            id: txtID
            anchors.centerIn: parent
            text: "default"

        }
        Keys.onPressed:  {
            if(event.key===Qt.Key_1){
                txtID.text="Pressed key 1"

            }else if(event.key===Qt.Key_6){
                txtID.text="Pressed key 6"
            }else{

                txtID.text="Pressed on another key "+event.key
            }
        }
    }

}
