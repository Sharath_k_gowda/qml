import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Rest API")

    function fetchJokes(url,callback){
        var xhr=new XMLHttpRequest();
        xhr.onreadystatechange=function(){
            if(xhr.readyState===XMLHttpRequest.HEADERS_RECEIVED){
                print("HEADERS_RECEIVED");
            }else if(xhr.readyState===XMLHttpRequest.DONE){
                print('done')
                if(xhr.status===200){
                    console.log("resource found")
                    callback(xhr.responseText.toString())
                }else{
                    callback(null);

                }

            }

        }
        xhr.open('GET',url);
        xhr.send();


    }

    ColumnLayout{
        anchors.fill: parent
        spacing: 0
        ListModel{
            id:listModelId


        }

        ListView{
            id:listViewID
            model: listModelId
            delegate: delegateId
            Layout.fillHeight: true
            Layout.fillWidth: true

        }

        SpinBox{
            id:spinBoxId
            Layout.fillWidth: true
            value: 10

        }

        Button{
            id:buttonId
            Layout.fillWidth: true
            text: "Fetch"
            onClicked: {
                fetchJokes("http://api.icndb.com/jokes/random/"+spinBoxId.value,function(response){
                    console.log(response)
                    if(response){
                        var object=JSON.parse(response)
                        object.value.forEach(function(joke){
                            listModelId.append({"joke":joke.joke})
                        })

                    }else{
                    console.log("something went wrong")
                    }
                }
                )
            }


        }
        Component{
            id:delegateId
            Rectangle{
                id:rectangleId
                width: parent.width
                height: textId.implicitHeight+30
                color: "beige"
                border.color: "yellowgreen"
                radius: 5








                Text {
                    id: textId
                    text: joke
                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent
                    font.pointSize: 13
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter



                }
            }
        }
    }

}
