import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.12
import com.intimetec.qml 1.0
import Logc 1.2

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("logs-app")

    Mylog{
        id:logid
    }
    Rectangle{
        anchors.fill: parent
        color: "black"
        Button{
            anchors.centerIn: parent
            text: "Warn"
            palette {
                button: "yellow"
            }
            MouseArea{
                anchors.fill: parent
                anchors.leftMargin: -16
                onClicked:{
                    logid.log(Style.Warn,'this is warn')

                }
            }
        }

        Button{
            x:257
            y:100
            text: "Info"
            palette {
                button: "green"
            }
            MouseArea{
                anchors.fill: parent
                anchors.rightMargin: 100
                onClicked:{

                    logid.log(Style.Info,'this is info')
                }
            }
        }

        Button {
            id: button
            x: 74
            y: 168
            text: "Debug"
            palette {
                button: "orange"
            }
            MouseArea{
                anchors.fill: parent
                anchors.rightMargin: 100
                onClicked:{

                    logid.log(Style.Debug,'this is debug')
                }
            }
        }

        Button {
            id: button1
            x: 459
            y: 168
            width: 106
            height: 34
            text: "Error"
            palette {
                button: "red"
            }

            MouseArea{
                anchors.fill: parent
                anchors.rightMargin: 100
                onClicked:{

                    logid.log(Style.Error,'this is Error')
                }
            }
        }


    }
}
