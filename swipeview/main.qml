import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Swipeview and page indicator")


    SwipeView{
        id:swipeViewId
        anchors.fill: parent
        currentIndex: pageIndicatorId.currentIndex
        Image {
            id: image1

            source: "https://lh3.googleusercontent.com/proxy/Krp79181N0t41j7HUh5gNVf0Jc5uCvR3gApTtCLX9k60N-TeLzGNCGmEM7b3Lsr8Lr71xikpWYupCwRgu35eq5__drmApq_JBOmLqkHNOfQ4Bw"
        }

        Image {
            id: image2

            source: "https://www.techworm.net/programming/wp-content/uploads/2018/09/cpppp-490x490.png"
        }
        Image {
            id: image3

            source: "https://miro.medium.com/max/8642/1*iIXOmGDzrtTJmdwbn7cGMw.png"
        }
        Image {
            id: image4

            source: "https://miro.medium.com/max/690/1*jsgLaIkhgF7SzQS1FWIPug.jpeg"
        }




    }
    PageIndicator{
        id:pageIndicatorId
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        currentIndex: swipeViewId.currentIndex
        interactive: true
        count: swipeViewId.count

    }



}
