import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Swipeview and page indicator")


    Row{
        spacing: 20
        width: parent.width
        x:20
        y:20

        RangeSlider{
            //orientation: Qt.Vertical
            from: 1
            to:100
            first.value: 25
            second.value: 75

            first.onValueChanged: {
                console.log(Math.floor(first.value))

            }
            second.onValueChanged:  {
                console.log(Math.floor(second.value))
            }

        }
    }
}
