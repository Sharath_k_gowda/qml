import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("progess bar")
    Column{
        width: parent.width
        spacing: 40

        Button{
            text: "Start"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                progressBarId.value=90
            }

        }
        Dial{
            anchors.horizontalCenter: parent.horizontalCenter
            from: 1
            to:100
            value: 40
            onValueChanged: {
                progressBarId.value=value;
            }


        }

        ProgressBar{
            id:progressBarId
            anchors.horizontalCenter: parent.horizontalCenter
            from: 1
            to:100

            value: 40
        }

        ProgressBar{
            id:progressBar2Id
            anchors.horizontalCenter: parent.horizontalCenter
            from: 1
            to:100
            indeterminate: true



            value: 40
        }
        Button{
            text: "stop"
            onClicked: {
                progressBar2Id.indeterminate=false;
            }
        }


    }


}
