#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include<QQuickStyle>
#include<QGuiApplication>
#include <QPalette>
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    // load our style

    QQuickStyle::setStyle("Material");
    QGuiApplication::setPalette(Qt::blue);

    const QUrl url(QStringLiteral("qrc:/BusyIndicate.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
