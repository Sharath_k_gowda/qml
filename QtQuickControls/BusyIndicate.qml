import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Styles 1.4

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Qt Quick Controls")


    ColumnLayout{
        width: parent.width
        // height: parent.height
        BusyIndicator{
            id:busyIndicatorId
            Layout.alignment: Qt.AlignHCenter
            running: false
            visible: false

        }
        ColumnLayout{
            Button{
                id:button1Id
                text: "Run"

                Layout.fillWidth: true
                onClicked: {
                    busyIndicatorId.running=true
                    busyIndicatorId.visible=true
                }

            }
            Button{
                id:button2Id
                text: "Stop"
                Layout.fillWidth: true
                onClicked: {
                    busyIndicatorId.running=false
                     busyIndicatorId.visible=false

                }
            }
        }
    }
}
