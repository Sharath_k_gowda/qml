import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Styles 1.4

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Qt Quick Controls")


    RowLayout{
        anchors.centerIn: parent
        spacing: 30
        Button{
            id:button1Id
            text: "Ok"


            palette {
                button: "red"
                buttonText: "red"
            }

            onClicked: {
                console.log("Clicked on OK button")

            }
            onDoubleClicked: {
                console.log("DoubleClicked on OK button")
            }
        }
        Button{
            id:button2Id
            text: "Cancel"

            onClicked: {
                console.log("Clicked on Cancel button")

            }
            onDoubleClicked: {
                console.log("DoubleClicked on Cancel button")
            }
        }
        Button{
            id:button3Id
            text: "Login"
        }
    }
}
